//! # Actors on Stage
//! Stage is an Actor Model. That is to say, it's a means to define a system composed of units of
//! work that communicate via messages.
use std::time::{Duration, SystemTime};

pub use futures;
use once_cell::sync::Lazy;
pub use tracing;
use tracing::error;

pub use utils::sys_msgs;

#[deny(missing_docs)]
pub(crate) mod utils;

pub mod actors;
pub mod system;
pub mod traits;

pub type StdError = Box<dyn std::error::Error + Send + Sync + 'static>;
/// Stage's Epoch for ID generation (January 1st, 2020)
pub static STAGE_EPOCH: Lazy<SystemTime> =
    Lazy::new(|| SystemTime::UNIX_EPOCH + Duration::from_secs(1_576_800_000));

/// A simple panic hook that passes panics into `tracing`.
pub fn trace_panic_hook() {
    std::panic::set_hook(Box::new(|val| {
        if let Some(s) = val.payload().downcast_ref::<&str>() {
            error!(
                "Panic occured at {}:L{} - {}",
                val.location().unwrap().file(),
                val.location().unwrap().line(),
                s
            );
        } else {
            error!(
                "Panic occured at {}:L{}",
                val.location().unwrap().file(),
                val.location().unwrap().line()
            );
        }
    }))
}

// TODO: Add tests. All the tests.
