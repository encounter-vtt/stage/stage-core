/// Messages used by Stage. These messages should be taken seriously and most often not ignored.
pub mod sys_msgs;
pub mod unshared;
