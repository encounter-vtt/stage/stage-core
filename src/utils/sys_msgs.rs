use std::cell::UnsafeCell;

use tokio::sync::{oneshot, oneshot::Sender};

use crate::{
    actor_msg,
    actors::{Actor, ActorError, ActorRef, Message, MsgError, Response},
};

/// Informs all Actors in the System that the System has properly started and main work is ready to
/// begin.
pub struct SystemStarted;

/// Informs the Actor that it has started. This gives it a chance to perform actions before
/// receiving any other messages.
pub struct ActorStart;

/// On receipt of this, the Actor should do any last-minute work. It will not receive any messages
/// after this.
pub struct StopCmd;

/// Message sent to monitors when an Actor they monitor has died.
pub struct MonitorAlert {
    /// Id of the Actor that has died.
    pub id: ActorRef,
    /// The error returned by the Actor.
    pub err: ActorError,
}

/// Message sent to monitors when they replace an existing monitor. Receiving this puts the
/// responsibility of the monitor to forward alerts to this monitor once it's done.
pub struct ForwardAlert {
    /// The Actor that needs to be forwarded to.
    pub monitor: Actor,
}

/// Message sent when the Actor is expected to give a response.
pub struct Request {
    msg: Message,
    res: UnsafeCell<Option<ResponseBox>>,
}

// TODO: Safety audit
// Should be safe as long as ResponseBox is...
unsafe impl Send for Request {}
unsafe impl Sync for Request {}

pub(crate) enum ResponseBox {
    Future(Sender<Message>),
    Handle(Actor),
}

impl Request {
    /// Getter for the message sent as the request
    pub fn msg(&self) -> &Message {
        &self.msg
    }

    /// Sends the response back to the requester
    ///
    /// # Panics
    /// This function will panic if more than one response is attempted.
    pub async fn respond<M: Into<Message>>(&self, msg: M) -> Result<(), MsgError> {
        // Safety: this is safe because the Option within the cell
        let res = unsafe { (*self.res.get()).take() };
        res.expect("Attempted to send response twice").send(msg).await
    }

    /// Responds with a question.
    ///
    /// # Panics
    /// This function will panic if more than one response is attempted.
    pub async fn ask_back<M: Into<Message>>(
        &self, msg: M, mode: Response,
    ) -> Result<Option<Message>, MsgError> {
        match mode {
            Response::Wait => {
                let (tx, rx) = oneshot::channel();
                let req = Request::new(msg, ResponseBox::Future(tx));

                let res = unsafe { (*self.res.get()).take() };
                res.expect("Attempted to send response twice").send(req).await?;

                rx.await.map(Some).map_err(|_| MsgError::NoResponseSent)
            }
            Response::Mail(actor) => {
                let req = Request::new(msg, ResponseBox::Handle(actor));

                let res = unsafe { (*self.res.get()).take() };
                res.expect("Attempted to send response twice").send(req).await?;

                Ok(None)
            }
        }
    }

    pub(crate) fn new<M: Into<Message>>(msg: M, res: ResponseBox) -> Self {
        let msg = msg.into();
        let res = UnsafeCell::new(Some(res));

        Self { msg, res }
    }
}

impl ResponseBox {
    async fn send<M: Into<Message>>(self, msg: M) -> Result<(), MsgError> {
        match self {
            ResponseBox::Future(tx) => tx.send(msg.into()).map_err(|_| MsgError::ActorIsStopped),
            ResponseBox::Handle(actor) => actor.send(msg).await,
        }
    }
}

actor_msg!(SystemStarted, ActorStart, StopCmd, MonitorAlert, ForwardAlert, Request);
