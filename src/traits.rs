use std::{any::Any, future::Future};

use crate::{
    actors::{ActorCtx, ActorResult, Message},
    system::ActorSystemBuilder,
};

/// The interface to define performance-critical background drivers.
///
/// # For extension use only
/// The Actor Model is intended to be ran as a gilded prison, however sometimes things need to be
/// made to drive said prison, such as for networking or rendering. Outside of those and similar
/// cases, it is advised to stay within the Actor Model. If you need to do constant iterations of
/// work, try having an Actor conclude an iteration by sending itself a message to start another.
///
/// # Threading
/// Each StageReactor uses a thread of its own, but they are constructed within that thread. They
/// do not need to be Send or Sync.
pub trait StageReactor {
    /// This defines the type of payload that can be passed during creation of the [`StageReactor`].
    /// This is required because creation is done automatically.
    type Config: Send + 'static;
    /// The name is prefixed with the [`ActorSystem`] identifier and suffixed with an incrementer.
    /// Given the `THREAD_NAME` "NetworkDriver", a potential thread name is
    /// "ActorSystemName-NetworkDriver-5".
    const THREAD_NAME: &'static str;

    /// Constructor function for the [`StageReactor`]. This is called internally when spooling the
    /// reactor.
    fn new(config: Self::Config) -> Self;

    /// The primary logic for the reactor. Returns whether the reactor should continue or break.
    ///
    /// The reactor will not receive any indication of shutdown, and can't rely on knowing that the
    /// system is shutting down. Implement any cleanup in [`std::ops::Drop`].
    fn iter(&mut self) -> Loop;
}

/// Defining Loop hatches as a return-able value.
pub enum Loop {
    Continue,
    Break,
}

/// An interface defining an extension of Stage. Intended for ease of composition of an extended
/// [`ActorSystem`].
pub trait StageExtension {
    /// Error type of the [`StageExtension::Error`] method.
    type Error;
    /// Function that the user should run to add the extension to the [`ActorSystem`].
    fn extend(system: &mut ActorSystemBuilder) -> Result<(), Self::Error>;
}

// TODO: Find good documentation on FSMs
/// Actors are nothing more than a specific type of function paired with state. They receive a
/// message, mutate their state, send messages of their own, make external calls, etc. Almost
/// universally, Actors are [Finite-State Machines]()
///
/// `S` - The state of the Actor.
/// `F` - The async result of the Actor.
pub trait ActorFn<S: Send + 'static, F: Future<Output = ActorResult<S>> + Send>:
    (FnMut(S, ActorCtx, Message) -> F) + Send
{
}

impl<S, F, T> ActorFn<S, F> for T
where
    S: Send + 'static,
    F: Future<Output = ActorResult<S>> + Send,
    T: (FnMut(S, ActorCtx, Message) -> F) + Send,
{
}

// TODO: attribute to derive w/ options.
/// Defines a message's bounds. In the future, this may extend for support for clusters.
pub trait ActorMessage: 'static + Send + Sync + Any {}

#[macro_export]
macro_rules! actor_msg {
    ($($t:ident),+) => {
        $(impl $crate::traits::ActorMessage for $t {})+
    }
}
