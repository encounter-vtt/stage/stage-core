use std::{
    error::Error,
    fmt::{self, Display, Formatter},
    io,
};

use crossbeam_channel::Sender;
use once_cell::sync::OnceCell;
use tokio::sync::oneshot;

use crate::{
    actors::{Actor, ActorRef, BuildActorOp},
    system::{handle::internal_futures::NewActorFuture, reactor::SysOp},
};

pub mod internal_futures;

static CORE_CHANNEL: OnceCell<Sender<SysOp>> = OnceCell::new();

pub type IoResult<T> = Result<T, std::io::Error>;

#[derive(Clone)]
pub struct SysHandle;

impl SysHandle {
    pub(crate) fn init(sender: Sender<SysOp>) {
        let _ = CORE_CHANNEL.set(sender);
    }

    /// Submits a request to spawn a new Actor, returning a Future that will yield the Actor handle.
    pub fn new_actor(&self, op: BuildActorOp) -> NewActorFuture {
        let (tx, rx) = oneshot::channel();

        let payload = SysOp::NewActor { data: op, mailbox: tx };

        let _ = CORE_CHANNEL.get().unwrap().clone().send(payload); // CoreReactor is the last thing to stop.

        NewActorFuture::new(rx)
    }

    /// Sends a stop message to the system. This is will stop all Reactors, so don't count on any
    /// Actors running after this is sent.
    pub fn stop_system(&self) {
        let _ = CORE_CHANNEL.get().unwrap().clone().send(SysOp::StopSystem);
    }

    /// Has `monitor` receive an error message when `actor` is stopped or errors.
    pub fn monitor_actor(&self, actor: ActorRef, monitor: Actor) {
        let _ = CORE_CHANNEL.get().unwrap().clone().send(SysOp::MonitorActor { actor, monitor });
    }

    /// Performs a hard-stop of the `actor`. It will be dropped potentially while still handling a
    /// message, if in a Pending async state.
    pub fn stop_actor(&self, actor: ActorRef) {
        let _ = CORE_CHANNEL.get().unwrap().clone().send(SysOp::StopActor { actor });
    }
}

#[derive(Debug)]
pub enum CoreError {
    ActorNameTaken,
    IoError(io::Error),
}

impl Error for CoreError {}

impl Display for CoreError {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        write!(f, "{:?}", self)
    }
}
