use std::{
    borrow::Cow,
    collections::HashMap,
    sync::{
        atomic::{AtomicBool, Ordering},
        Arc,
    },
    time::{Duration, SystemTime},
};

use dashmap::DashMap;
use futures::stream::StreamExt;
use once_cell::sync::Lazy;
use tokio::sync::oneshot::Sender as SendOnce;
use tracing::{error, info};

use crate::{
    actors::{stream::ActorStream, Actor, ActorCtx, ActorRef, BuildActorOp},
    system::{handle::CoreError, SysHandle},
    traits::{Loop, StageReactor},
    utils::sys_msgs::{ActorStart, ForwardAlert, MonitorAlert},
};
use crossbeam_channel::RecvTimeoutError;
use tokio::runtime::Handle;

static MONITORS: Lazy<DashMap<ActorRef, Actor>> = Lazy::new(DashMap::new);

/// Driver for all internal operations. This is responsible for taking system requests from Actors
/// and executing them. Such requests include but are not limited to spawning Actors, shutting down
/// the system, and registering Monitors.
pub struct CoreReactor {
    actor_handles: Vec<Actor>,
    actor_incrementer: u16,
    inbound: crossbeam_channel::Receiver<SysOp>,
    iter_timeout: Duration,
    name_lookup: HashMap<Cow<'static, str>, Actor>,
    shutdown_flag: Arc<AtomicBool>,
    rt_handle: Handle,
}

/// Payload for requesting operations of the [`CoreReactor`].
pub enum SysOp {
    NewActor { data: BuildActorOp, mailbox: SendOnce<Result<Actor, CoreError>> },
    StopSystem,
    MonitorActor { actor: ActorRef, monitor: Actor },
    StopActor { actor: ActorRef },
}

impl StageReactor for CoreReactor {
    type Config = CoreReactorConfig;
    const THREAD_NAME: &'static str = "CoreReactor";

    fn new(config: Self::Config) -> Self {
        let Self::Config { inbound, iter_timeout, shutdown_flag, rt_handle } = config;

        Self {
            inbound,
            iter_timeout,
            shutdown_flag,
            actor_handles: vec![],
            actor_incrementer: 0,
            name_lookup: Default::default(),
            rt_handle,
        }
    }

    fn iter(&mut self) -> Loop {
        match self.inbound.recv_timeout(self.iter_timeout) {
            Ok(op) => self.process(op),
            Err(e) => match e {
                RecvTimeoutError::Timeout => Loop::Continue,
                _ => Loop::Break,
            },
        }
    }
}

impl CoreReactor {
    fn process(&mut self, op: SysOp) -> Loop {
        match op {
            SysOp::NewActor { data, mailbox } => self.new_actor(data, mailbox),
            SysOp::StopSystem => self.stop_system(),
            SysOp::MonitorActor { actor, monitor } => self.monitor_actor(actor, monitor),
            SysOp::StopActor { actor } => Loop::Continue,
        }
    }

    fn new_actor(
        &mut self, data: BuildActorOp, mailbox: SendOnce<Result<Actor, CoreError>>,
    ) -> Loop {
        info!("Spawning new Actor");
        if mailbox.send(self.inner_new_actor(data)).is_err() {
            error!("Attempted to notify NewActorFuture, but it was dropped");
        }

        Loop::Continue
    }

    fn gen_actor_id(&mut self) -> u64 {
        let now = SystemTime::now().duration_since(*crate::STAGE_EPOCH).unwrap().as_millis() as u64;
        let id = (now << 20) | self.actor_incrementer as u64;
        self.actor_incrementer = self.actor_incrementer.overflowing_add(1).0;
        id
    }

    fn monitor_actor(&mut self, actor: ActorRef, monitor: Actor) -> Loop {
        if let Some(m) = MONITORS.get_mut(&actor) {
            let _ = monitor.send(ForwardAlert { monitor: m.clone() });
        }

        MONITORS.insert(actor, monitor);

        Loop::Continue
    }

    #[allow(clippy::mutex_atomic)]
    fn stop_system(&mut self) -> Loop {
        info!("Setting stop flag");
        let _ = self.shutdown_flag.swap(true, Ordering::Acquire);

        Loop::Break
    }

    fn inner_new_actor(&mut self, data: BuildActorOp) -> Result<Actor, CoreError> {
        let BuildActorOp { name, wrapper } = data;

        let mut named = false;

        if let Some(name) = &name {
            named = true;
            if self.name_lookup.contains_key(name) {
                return Err(CoreError::ActorNameTaken);
            }
        }

        let actor_ref = ActorRef { id: self.gen_actor_id(), name: name.clone() };
        let (msg_tx, msg_rx) = tokio::sync::mpsc::channel(512);

        let handle = Actor { mailbox: msg_tx, reference: actor_ref };

        let ctx = ActorCtx { this: handle.clone(), sys: SysHandle };

        tokio::spawn(async {
            let mut stream = ActorStream::new(ctx, wrapper, msg_rx);
            while let Some(res) = stream.next().await {
                if let Err(err) = res {
                    if let Some(m) = MONITORS.get(&stream.ctx.this.reference) {
                        let _ = m
                            .value()
                            .send(MonitorAlert { id: stream.ctx.this.reference.clone(), err });
                    }
                    break;
                }
            }
        });

        let handle2 = handle.clone();
        self.rt_handle.spawn(async move { handle2.send(ActorStart).await });

        if named {
            self.name_lookup.insert(name.unwrap(), handle.clone());
        }

        self.actor_handles.push(handle.clone());
        Ok(handle)
    }
}

impl Drop for CoreReactor {
    fn drop(&mut self) {
        info!("CoreReactor dropping")
    }
}

pub struct CoreReactorConfig {
    pub(crate) inbound: crossbeam_channel::Receiver<SysOp>,
    pub(crate) iter_timeout: Duration,
    pub(crate) shutdown_flag: Arc<AtomicBool>,
    pub(crate) rt_handle: Handle,
}
