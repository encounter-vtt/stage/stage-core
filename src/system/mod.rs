use std::{
    borrow::Cow,
    collections::VecDeque,
    sync::{
        atomic::{AtomicBool, Ordering},
        Arc,
    },
    thread,
    time::Duration,
};

use tokio::runtime::{Builder, Handle, Runtime};
use tracing::{debug, error, info};

pub use crate::system::{config::ActorSystemConfig, handle::SysHandle};
use crate::{
    actors::BuildActorOp,
    system::{
        handle::CoreError,
        reactor::{CoreReactor, CoreReactorConfig},
    },
    traits::{Loop, StageExtension, StageReactor},
};

mod config;
mod handle;
mod reactor;

/// The central system that defines a node of Actors.
pub struct ActorSystem {
    shutdown_flag: Arc<AtomicBool>,
    config: ActorSystemConfig,
}

pub struct ActorSystemBuilder {
    config: ActorSystemConfig,
    shutdown_flag: Arc<AtomicBool>,
    starting_actors: VecDeque<BuildActorOp>,
    tokio_rt: Handle,
}

impl ActorSystem {
    /// Gets the [`SysHandle`] used to send commands to the internal CoreReactor. Primary means of
    /// controlling the ActorSystem.
    pub fn get_handle(&self) -> SysHandle {
        SysHandle
    }

    /// Blocks the current thread until the ActorSystem has shutdown. `None` will block indefinitely.
    /// After this, [`tokio::runtime::Runtime::shutdown_timeout`] should be invoked.
    pub fn await_shutdown<T: Into<Option<Duration>>>(self, timeout: T) {
        info!("Setting stop flag");
        let _ = self.shutdown_flag.swap(true, Ordering::Acquire);
        // TODO: Add barrier w/ Reactors
    }
}

impl ActorSystemBuilder {
    /// Starts the process of building the ActorSystem.
    pub fn new(config: ActorSystemConfig, tokio_handle: Handle) -> Self {
        let (core_tx, core_rx) = crossbeam_channel::bounded(512);

        SysHandle::init(core_tx);

        let shutdown_flag: Arc<AtomicBool> = Default::default();

        let core_config = CoreReactorConfig {
            inbound: core_rx,
            iter_timeout: Duration::from_millis(50), // TODO: Expose in config
            shutdown_flag: shutdown_flag.clone(),
            rt_handle: tokio_handle.clone(),
        };

        let mut sys = Self {
            config,
            shutdown_flag,
            starting_actors: Default::default(),
            tokio_rt: tokio_handle,
        };

        info!("Spawning core reactor");
        sys.add_reactor::<CoreReactor>(core_config);

        sys
    }

    /// Add a [`StageReactor`] to the System. See trait for more details.
    /// # Note
    /// This function will spawn a thread for that Reactor to run on.
    pub fn add_reactor<T: StageReactor>(&mut self, config: T::Config) {
        debug!("Spawning reactor {}", T::THREAD_NAME);
        let flag = self.shutdown_flag.clone();
        let handle = self.tokio_rt.clone();
        thread::spawn(move || {
            handle.enter(|| {
                let mut reactor = T::new(config);
                while !flag.load(Ordering::Relaxed) {
                    if let Loop::Break = reactor.iter() {
                        debug!("Reactor {} is shutting down", T::THREAD_NAME);
                        break;
                    }
                }
            })
        });
    }

    /// Add a [`StageExtension`] to the System. See trait for more details.
    pub fn add_extension<T: StageExtension>(&mut self) -> Result<(), T::Error> {
        T::extend(self)
    }

    /// Adds the build operation to a pre-start queue. Actors will be built with the ActorSystem,
    /// and not before.
    pub fn add_actor(&mut self, op: BuildActorOp) {
        self.starting_actors.push_back(op)
    }

    /// Builds the ActorSystem and uses the rest of the threads available as ActorReactors. Returns
    /// after the ActorSystem has built and initial Actors are started.
    #[allow(clippy::block_in_if_condition_stmt)]
    pub async fn start(self) -> Result<ActorSystem, CoreError> {
        let ActorSystemBuilder { config, shutdown_flag, starting_actors, tokio_rt: _tokio_rt } =
            self;
        let sys = ActorSystem { shutdown_flag, config };

        let mut checked: Vec<Cow<'static, str>> = vec![];
        if starting_actors
            .iter()
            .filter(|op| op.name.is_some())
            .map(|op| op.name.clone().unwrap())
            .any(|op| {
                let check = checked.iter().any(|c| op == *c);
                if !check {
                    checked.push(op.clone());
                }
                check
            })
        {
            return Err(CoreError::ActorNameTaken);
        }
        drop(checked);

        let handle = sys.get_handle();
        for op in starting_actors.into_iter() {
            let _ = handle.new_actor(op).await;
        }

        Ok(sys)
    }
}

#[derive(Clone, Debug)]
pub struct OutOfThreads;

//pub(crate) struct ShutdownFlag {}

/// Defines the mode when issuing a shutdown
pub enum StopMode {
    Immediately,
    WaitForCompletion,
    WaitFor(Duration),
}
