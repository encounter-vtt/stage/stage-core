use std::{
    borrow::BorrowMut,
    future::Future,
    pin::Pin,
    task::{Context, Poll},
};

use pin_utils::pin_mut;
use tokio::sync::oneshot::Receiver;

use crate::{actors::Actor, system::handle::CoreError};

pub struct NewActorFuture {
    mailbox: Receiver<Result<Actor, CoreError>>,
}

impl NewActorFuture {
    pub(crate) fn new(mailbox: Receiver<Result<Actor, CoreError>>) -> Self {
        Self { mailbox }
    }
}

impl Future for NewActorFuture {
    type Output = Result<Actor, CoreError>;

    fn poll(mut self: Pin<&mut Self>, cx: &mut Context) -> Poll<Self::Output> {
        let mailbox = self.mailbox.borrow_mut();
        pin_mut!(mailbox);
        mailbox.poll(cx).map(|res| res.unwrap())
    }
}
