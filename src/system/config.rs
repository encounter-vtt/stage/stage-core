pub struct ActorSystemConfig {
    /// Name for the ActorSystem. Used as a thread name prefix.
    pub(crate) name: Option<String>,
}

impl ActorSystemConfig {
    /// Sets the name of the ActorSystem. This is used as a thread prefix as well as for other
    /// display/debug purposes. Default: None
    pub fn name<N: Into<String>>(mut self, name: N) -> Self {
        self.name = Some(name.into());
        self
    }
}

impl Default for ActorSystemConfig {
    fn default() -> Self {
        Self { name: None }
    }
}
