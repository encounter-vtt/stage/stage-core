use std::{
    fmt::{self, Display, Formatter},
    sync::{atomic, atomic::AtomicBool, Arc},
    time::Duration,
};

use tokio::sync::{mpsc::Sender, oneshot};
use tracing::debug;

use crate::{
    actors::{error::MsgError, message::Message, ActorRef},
    utils::sys_msgs::{Request, ResponseBox},
};

/// A public handle to a specific Actor. This is what you Clone to pass around and use to send
/// messages to the Actor.
#[derive(Clone)]
pub struct Actor {
    pub(crate) mailbox: Sender<Message>,
    pub(crate) reference: ActorRef,
}

impl Actor {
    /// Send a Message to the Actor represented by this handle. Queues the Actor for processing if
    /// it is not already.
    pub async fn send<M: Into<Message>>(&self, msg: M) -> Result<(), MsgError> {
        let msg = msg.into();

        if self.mailbox.clone().send(msg).await.is_err() {
            debug!("Attempted to send message to Actor [{}], but it was stopped", self.reference);
            return Err(MsgError::ActorIsStopped);
        }

        Ok(())
    }

    /// Sends a message after a delay. On success, returns an Atomic flag indicating that the
    /// message has actually been sent.
    pub fn send_after<M: Into<Message>>(
        &self, msg: M, delay: Duration,
    ) -> Result<Arc<AtomicBool>, MsgError> {
        let flag = Arc::new(AtomicBool::default());
        let flag2 = flag.clone();

        let recp = self.clone();
        let msg = msg.into();

        tokio::spawn(async move {
            tokio::time::delay_for(delay).await;
            let _ = recp.send(msg).await;
            let _ = flag2.swap(true, atomic::Ordering::Acquire);
        });

        Ok(flag)
    }

    /// Sends a message with the expectation of a response. The [`Response`] mode can be either
    /// `Wait`, which asynchronously blocks on the receipt of a response, or `Mail(Actor)` which
    /// returns after sending and has the response sent to the given [`Actor`].
    pub async fn ask<M: Into<Message>>(
        &self, msg: M, mode: Response,
    ) -> Result<Option<Message>, MsgError> {
        match mode {
            Response::Wait => {
                let (tx, rx) = oneshot::channel();
                let req = Request::new(msg, ResponseBox::Future(tx));
                debug!("Sending Request message");
                self.send(req).await?;

                rx.await.map(Some).map_err(|_| MsgError::NoResponseSent)
            }
            Response::Mail(actor) => {
                let req = Request::new(msg, ResponseBox::Handle(actor));
                self.send(req).await.map(|_| None)
            }
        }
    }

    /// Gets a reference to the globally-unique reference. Useful for cheap comparisons.
    pub fn get_ref(&self) -> &ActorRef {
        &self.reference
    }
}

/// Sets the means of returning the response. For more details, see [`Actor::ask`].
pub enum Response {
    /// Asynchronously blocks on a Future response.
    Wait,
    /// Has the response go to the standard mailbox for the given Actor.
    Mail(Actor),
}

impl Display for Actor {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        write!(f, "{}", self.reference)
    }
}

impl PartialEq for Actor {
    fn eq(&self, other: &Self) -> bool {
        self.reference == other.reference
    }
}

impl Eq for Actor {}
