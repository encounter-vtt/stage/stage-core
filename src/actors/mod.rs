//! # Actors
//! TODO: Describe the Actor Model, the role of Actors and how to think about Actors, and lastly how
//! Actors should be used in Stage.

use std::{
    borrow::Cow,
    fmt::{self, Display, Formatter},
    hash::{Hash, Hasher},
};

pub use builder::BuildActorOp;
pub use context::ActorCtx;
pub use error::{ActorError, MsgError};
pub use handle::{Actor, Response};
pub use message::Message;

mod builder;
mod context;
mod error;
mod handle;
mod message;
pub(crate) mod stream;

/// Result for Actors, where `S` is the State of the Actor.
pub type ActorResult<S> = Result<S, error::ActorError>;

/// Reference to a specific Actor.
#[derive(Clone)]
pub struct ActorRef {
    /// Identifier of the Actor, unique within its System.
    pub(crate) id: u64,
    /// Debug/human readable identifier of the Actor.
    pub(crate) name: Option<Cow<'static, str>>,
}

impl Hash for ActorRef {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.id.hash(state);
    }
}

impl PartialEq for ActorRef {
    fn eq(&self, other: &Self) -> bool {
        self.id == other.id
    }
}

impl Eq for ActorRef {}

impl Display for ActorRef {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match &self.name {
            Some(name) => write!(f, "{}", name)?,
            None => write!(f, "{}", self.id)?,
        }

        Ok(())
    }
}
