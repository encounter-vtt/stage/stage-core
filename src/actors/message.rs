use std::any::{Any, TypeId};

use crate::traits::ActorMessage;

/// A unit of communication between Actors. A message should contain the minimum amount of
/// information for the recipient to function.
#[derive(Debug)]
pub struct Message {
    id: TypeId,
    val: Box<dyn Any + Send + Sync + 'static>,
}

impl Message {
    /// Attempts to get the inner [`ActorMessage`].
    pub fn try_cast<T: ActorMessage>(&self) -> Option<&T> {
        match self.id == TypeId::of::<T>() {
            false => None,
            true => self.val.as_ref().downcast_ref::<T>(),
        }
    }

    pub fn is<T: ActorMessage>(&self) -> bool {
        self.id == TypeId::of::<T>()
    }

    pub fn unwrap_as<T: ActorMessage>(self) -> Box<T> {
        let Self { val, .. } = self;
        (val as Box<dyn Any + Send + 'static>).downcast().expect("Message was not the given type")
    }
}

impl<T: ActorMessage> From<T> for Message {
    fn from(val: T) -> Self {
        Self { id: TypeId::of::<T>(), val: Box::new(val) }
    }
}
