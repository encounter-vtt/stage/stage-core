use std::{
    borrow::Cow,
    cell::UnsafeCell,
    panic::{catch_unwind, AssertUnwindSafe},
    ptr,
};

use futures::{Future, FutureExt};
use tracing::warn;

use crate::{
    actors::{
        context::ActorCtx,
        error::ActorError,
        message::Message,
        stream::{ActorWrapper, WrapperFuture},
    },
    traits::ActorFn,
};

/// Represents an operation payload to construct a new Actor.
pub struct BuildActorOp {
    pub(crate) name: Option<Cow<'static, str>>,
    pub(crate) wrapper: Box<dyn ActorWrapper>,
}

impl BuildActorOp {
    /// Takes an optional Name, a State, and an [`ActorFn`], and composes a payload for the System
    /// to use for creating a new Actor. Send the op via [`SysHandle::new_actor`].
    pub fn new<N, S, F, A>(name: N, state: S, mut actor_fn: A) -> Self
    where
        N: Into<Option<Cow<'static, str>>>,
        S: Send + Sync + 'static,
        F: Future<Output = Result<S, ActorError>> + Send + 'static,
        A: ActorFn<S, F> + 'static,
    {
        let name = name.into();

        let state_box = SendUnsafeCell(UnsafeCell::new(Some(state)));
        let wrapper = Box::new(move |ctx: ActorCtx, msg: Message| {
            let state = SendPointer(state_box.0.get());
            let s = match unsafe { (*state.0).take() } {
                Some(s) => s,
                None => panic!("[{}] state cell was empty", ctx.this.reference.id),
            };

            let future = catch_unwind(AssertUnwindSafe(|| (actor_fn)(s, ctx, msg)));

            Box::pin(async move {
                match future {
                    Ok(future) => match AssertUnwindSafe(future).catch_unwind().await {
                        Ok(x) => x,
                        Err(_) => {
                            warn!("Actor panicked! Catching as error");
                            Err(ActorError::Panicked)
                        }
                    },
                    Err(_) => {
                        warn!("Actor panicked! Catching as error");
                        Err(ActorError::Panicked)
                    }
                }
                .map(|s| {
                    unsafe { ptr::write(state.0, Some(s)) };
                })
            }) as WrapperFuture
        });

        Self { name, wrapper }
    }
}

#[repr(transparent)]
struct SendPointer<T>(*mut T);
#[repr(transparent)]
struct SendUnsafeCell<T>(UnsafeCell<T>);

unsafe impl<T> Send for SendPointer<T> {}
unsafe impl<T> Send for SendUnsafeCell<T> {}
