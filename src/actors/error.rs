use std::{
    error::Error,
    fmt::{self, Display, Formatter},
};

use crate::StdError;

/// Error returned by the Actor processor.
#[derive(Debug)]
pub enum ActorError {
    /// The Actor is stopping, don't run it again.
    Stopping,
    /// The Actor panicked. Actor panics are caught so they don't take down the whole System.
    Panicked,
    /// The Actor attempted to communicate with another Actor, but something went wrong.
    MsgError(MsgError),
    /// The Actor failed to handle an error. This is considered fatal.
    StdError(StdError),
}

/// Error returned when performing operations pre-processing or sending messages.
#[derive(Debug)]
pub enum MsgError {
    /// The [`Actor`] is stopped. Stopped [`Actor`]s can't receive messages.
    ActorIsStopped,
    /// The [`Actor`] that was expected to respond failed to do so before dropping the request.
    NoResponseSent,
}

impl Display for ActorError {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match self {
            ActorError::Stopping => write!(f, "Work concluded"),
            ActorError::Panicked => write!(f, "Panic!"),
            ActorError::MsgError(e) => write!(f, "Error on Actor messaging: {}", e),
            ActorError::StdError(e) => write!(f, "Error was not handled: {}", e),
        }
    }
}

impl Display for MsgError {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match self {
            MsgError::ActorIsStopped => write!(f, "Actor is stopped"),
            MsgError::NoResponseSent => write!(f, "Actor dropped the request before responding"),
        }
    }
}

impl Error for ActorError {}

impl Error for MsgError {}
