use std::{future::Future, pin::Pin};

use futures::{
    task::{Context, Poll},
    Stream,
};
use tokio::sync::mpsc;
use tracing::{debug, trace};

use crate::actors::{context::ActorCtx, error::ActorError, message::Message};

/// The stream of Actor message processing. `Poll::Ready(None)` corresponds to "this Actor has no
/// more pending messages". `Poll::Ready(Err)` means the Actor is stopped, regardless of why.
pub(crate) struct ActorStream {
    pub ctx: ActorCtx,
    handler: Box<dyn ActorWrapper>,
    mailbox: mpsc::Receiver<Message>,
    pending: Option<WrapperFuture>,
}

impl ActorStream {
    pub fn new(
        ctx: ActorCtx, handler: Box<dyn ActorWrapper>, mailbox: mpsc::Receiver<Message>,
    ) -> Self {
        Self { ctx, handler, mailbox, pending: None }
    }
}

impl Stream for ActorStream {
    type Item = Result<(), ActorError>;

    fn poll_next(self: Pin<&mut Self>, cx: &mut Context) -> Poll<Option<Self::Item>> {
        let mut me = self.get_mut();
        if me.pending.is_none() {
            match me.mailbox.poll_recv(cx) {
                Poll::Ready(msg) => match msg {
                    Some(msg) => {
                        debug!("Actor [{}] is processing a message", me.ctx.this);
                        let future = (me.handler)(me.ctx.clone(), msg);
                        me.pending = Some(future);
                    }
                    None => unreachable!("There should always be a copy of the Actor handle"),
                },
                Poll::Pending => return Poll::Pending,
            }
        }

        debug!("Actor [{}] is polling...", me.ctx.this);
        let mut pending = me.pending.take().unwrap();
        match pending.as_mut().poll(cx) {
            Poll::Ready(res) => {
                trace!("Actor [{}] has completed a message, dropping Future", me.ctx.this);
                Poll::Ready(Some(res))
            }
            Poll::Pending => {
                trace!("Actor [{}] is pending", me.ctx.this);
                me.pending.replace(pending);
                Poll::Pending
            }
        }
    }
}

pub(crate) type WrapperFuture =
    Pin<Box<dyn Future<Output = Result<(), ActorError>> + Send + 'static>>;

pub(crate) trait ActorWrapper: (FnMut(ActorCtx, Message) -> WrapperFuture) + Send {}

impl<T> ActorWrapper for T where T: (FnMut(ActorCtx, Message) -> WrapperFuture) + Send {}
