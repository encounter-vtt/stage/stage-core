use crate::{actors::Actor, system::SysHandle};

/// Static context for Actors. Anything they need from the ActorSystem should be available here.
#[derive(Clone)]
pub struct ActorCtx {
    /// A copy of the handle to the Actor using the context. Useful for identifying to other Actors.
    pub this: Actor,
    /// A copy of the handle to the System. This is how the system is worked with from within.
    pub sys: SysHandle,
}

impl ActorCtx {
    pub(crate) fn get_id(&self) -> u64 {
        self.this.get_ref().id
    }
}
