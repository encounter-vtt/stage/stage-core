# Stage
[![Latest version](https://img.shields.io/crates/v/stage.svg)](https://crates.io/crates/stage)
[![Discord](https://img.shields.io/discord/652350171514011659?color=7289DA&label=Discord&logo=Discord)](https://discord.gg/UEZBEBF)  
_Stage is currently in Beta, performance may be an issue._
#### An ergonomic, composable Actor Model, for painless concurrency
Stage sets itself out to be different than the majority of Actor Models in Rust by putting an emphasis on ergonomics and easy development iteration. You shouldn't be slowed by the intrinsics of a framework. Stage makes compromises in how "Rusty" it feels in order to simplify design.

## Goals
- Impose less as a framework and more as a model.
- Maximum convenience without sacrificing performance.
- Preserve the safety of Rust without constricting designs that might be possible in other models.

## Contributing
We're glad to have contributions! Please make sure to open an issue before hand, so we can coordinate.
