use futures::executor::block_on;
use stage::actors::{Actor, ActorError, BuildActorOp, Response};
use stage::sys_msgs::{ActorStart, Request};
use stage::system::{ActorSystemBuilder, ActorSystemConfig};
use stage::{
    actor_msg,
    actors::{ActorCtx, ActorResult, Message},
};
use std::sync::Arc;
use std::time::{Duration, Instant};
use tokio::runtime;
use tokio::runtime::Handle;
use tokio::sync::{Barrier, Mutex, Semaphore};
use tracing::{debug, error, info, trace};

struct PingPong {
    id: u8,
    sum: u8,
    targets: Vec<Actor>,
    targets_src: Arc<Mutex<Vec<Actor>>>,
    start: Arc<Barrier>,
    end: Arc<Barrier>,
}

impl PingPong {
    fn new(
        id: u8, targets: Arc<Mutex<Vec<Actor>>>, start: Arc<Barrier>, end: Arc<Barrier>,
    ) -> Self {
        Self { id, sum: id, targets: Vec::new(), targets_src: targets, start, end }
    }

    async fn handle(mut self, ctx: ActorCtx, msg: Message) -> ActorResult<Self> {
        if msg.is::<Setup>() {
            debug!("{} is setting up", self.id);
            let targets = self.targets_src.lock().await;
            self.targets = targets.clone();
        } else if msg.is::<Start>() {
            self.start.wait().await;
            debug!("{} is starting", self.id);
            self.sum = 0;
            for a in self.targets.iter() {
                let this = ctx.this.clone();
                a.ask(Ping, Response::Mail(this)).await.unwrap();
            }
            debug!("{} sent Pings", self.id);
        } else if let Some(req) = msg.try_cast::<Request>() {
            if req.msg().is::<Ping>() {
                debug!("{} received a Ping", self.id);
                req.respond(Pong(self.id)).await.unwrap();
                debug!("{} sent a Pong", self.id);
            } else {
                error!("{} received an unexpected Request: {:?}", self.id, req.msg());
            }
        } else if let Some(pong) = msg.try_cast::<Pong>() {
            self.sum += pong.0;
            debug!("{} is bumped by {} to {}", self.id, pong.0, self.sum);
        } else if msg.is::<ActorStart>() {
            trace!("{} received ActorStart", self.id);
        } else {
            error!("{} received unexpected message, {:?}", self.id, msg);
        }

        if self.sum == 136 {
            debug!("{} is awaiting the end", self.id);
            self.end.wait().await;
        }

        Ok(self)
    }
}

struct Ping;
struct Pong(u8);
struct Setup;
struct Start;

actor_msg!(Ping, Pong, Setup, Start);

pub fn main() {
    let sub =
        tracing_subscriber::FmtSubscriber::builder().with_max_level(tracing::Level::ERROR).finish();

    let _ = tracing::subscriber::set_global_default(sub);

    stage::trace_panic_hook();

    let mut tokio_rt = runtime::Builder::new()
        .threaded_scheduler()
        .enable_all()
        .build()
        .expect("Failed to build tokio runtime");

    tokio_rt.block_on(async_ping_pong(tokio_rt.handle().clone()));
}

pub async fn async_ping_pong(tokio_rt: Handle) {
    info!("Starting setup");
    let cfg = ActorSystemConfig::default();
    let sys = ActorSystemBuilder::new(cfg, tokio_rt).start().await.unwrap();

    let handle = sys.get_handle();

    let barrier_start = Arc::new(Barrier::new(16));
    let barrier_end = Arc::new(Barrier::new(17));
    let targets: Arc<Mutex<Vec<Actor>>> = Default::default();

    info!("Starting PingPong Actors");
    for i in 1u8..=16 {
        let start = barrier_start.clone();
        let end = barrier_end.clone();
        let targets2 = targets.clone();
        let build =
            BuildActorOp::new(None, PingPong::new(i, targets2, start, end), PingPong::handle);
        let actor = handle.new_actor(build).await.unwrap();

        targets.lock().await.push(actor);
    }

    let targets = targets.lock().await.clone();

    info!("Issuing startup commands");
    for a in targets.iter() {
        a.send(Setup).await.unwrap()
    }

    let mut runs = Vec::new();

    let iterations = 200;
    info!("Running {} iterations", iterations);
    for i in 0..iterations {
        info!("Iteration {:00}", i);
        let start = Instant::now();
        for a in targets.iter() {
            a.send(Start).await.unwrap();
        }
        debug!("Iteration awaiting the end");
        barrier_end.wait().await;
        runs.push(Instant::now() - start);
    }

    let mut runs = runs.into_iter().map(|d| d.as_nanos()).collect::<Vec<_>>();

    let mean = runs.iter().sum::<u128>() / runs.len() as u128;
    runs.sort();
    let min = *runs.get(0).unwrap();
    runs.reverse();
    let max = *runs.get(0).unwrap();
    println!("Mean run: {} ns", mean);
    println!(" Max run: {} ns", max);
    println!(" Min run: {} ns", min);
}
