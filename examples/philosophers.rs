use std::time::{Duration, SystemTime};

use futures::executor::block_on;
use tokio::{join, runtime};
use tracing::{debug, info};

use stage::{
    actor_msg,
    actors::{Actor, ActorCtx, ActorError, ActorResult, BuildActorOp, Message, Response},
    sys_msgs::{ActorStart, Request},
    system::{ActorSystemBuilder, ActorSystemConfig},
};
use std::sync::Arc;
use tokio::runtime::Handle;
use tokio::sync::Notify;

// START - State objects

struct Fork;

struct Philosopher {
    name: &'static str,
    forks: (Actor, Actor),
    times_eaten: u16,
    time_thinking: Duration,
    start_thinking: SystemTime,
    eating_time: Duration,
}

// END - State objects
// START - Message objects

struct Continue;

struct ForkGrant(Arc<Notify>);
struct ForkRequest;

struct StopSession;

actor_msg!(Continue, ForkGrant, ForkRequest, StopSession);

// END - Message objects

impl Fork {
    async fn handle(id: i32, _: ActorCtx, msg: Message) -> ActorResult<i32> {
        debug!("Fork {} got a message", id);
        if let Some(req) = msg.try_cast::<Request>() {
            if req.msg().try_cast::<ForkRequest>().is_some() {
                debug!("Fork {} granting usage", id);
                let notify = Arc::new(Notify::new());
                req.respond(ForkGrant(notify.clone())).await.unwrap();
                drop(msg);
                notify.notified().await;
                debug!("Fork {} released", id);
            }
        }

        Ok(id)
    }
}

impl Philosopher {
    async fn handle(mut self, ctx: ActorCtx, msg: Message) -> ActorResult<Self> {
        if msg.is::<StopSession>() {
            info!(
                "Philosopher {}'s survey | Total time thinking: {}ms | Servings eaten: {}",
                self.name,
                self.time_thinking.as_millis(),
                self.times_eaten,
            );
            return Err(ActorError::Stopping);
        } else if msg.is::<ActorStart>() {
            info!("Philosopher {} is seated at seat (id) {}", self.name, ctx.this.get_ref());
            self.start_thinking = SystemTime::now();
        }

        let (left_fork, right_fork) = &self.forks;

        // Get the forks
        debug!("Philosopher {} is getting forks", self.name);
        let (left_grant, right_grant) = join!(
            left_fork.ask(ForkRequest, Response::Wait),
            right_fork.ask(ForkRequest, Response::Wait)
        );

        let (left_grant, right_grant) =
            (left_grant.unwrap().unwrap(), right_grant.unwrap().unwrap());

        let (left_grant, right_grant) =
            (left_grant.unwrap_as::<ForkGrant>(), right_grant.unwrap_as::<ForkGrant>());

        // Collect our thoughts
        let time_thinking = SystemTime::now().duration_since(self.start_thinking).unwrap();
        self.time_thinking += time_thinking;

        // Eat
        debug!("Philosopher {} is starting to eat", self.name);
        // tokio::time::delay_for(self.eating_time).await;
        self.times_eaten += 1;

        // Return forks
        left_grant.0.notify();
        right_grant.0.notify();
        debug!("Philosopher {} has released the forks", self.name);

        // Continue thinking
        ctx.this.send(Continue).await.unwrap();
        self.start_thinking = SystemTime::now();

        Ok(self)
    }
}

pub fn main() {
    let sub =
        tracing_subscriber::FmtSubscriber::builder().with_max_level(tracing::Level::INFO).finish();

    let _ = tracing::subscriber::set_global_default(sub);

    stage::trace_panic_hook();

    let mut tokio_rt = runtime::Builder::new()
        .threaded_scheduler()
        .core_threads(6)
        .enable_all()
        .build()
        .expect("Failed to build tokio runtime");

    tokio_rt.block_on(async_main(tokio_rt.handle().clone()));

    tokio_rt.shutdown_timeout(Duration::from_secs(6));
}

async fn async_main(handle: Handle) {
    let cfg = ActorSystemConfig::default();
    let sys = ActorSystemBuilder::new(cfg, handle).start().await.unwrap();

    let handle = sys.get_handle();

    let forks = (0..5)
        .map(|i| {
            let build = BuildActorOp::new(None, i, Fork::handle);
            block_on(handle.new_actor(build)).unwrap()
        })
        .collect::<Vec<Actor>>();

    let philosophers = [
        ("Alpha", 0_usize, 1_usize),
        ("Beta", 1_usize, 2_usize),
        ("Gamma", 2_usize, 3_usize),
        ("Delta", 3_usize, 4_usize),
        ("Epsilon", 4_usize, 0_usize),
    ]
    .iter()
    .map(|(name, left, right)| {
        let left = forks.get(*left).unwrap().clone();
        let right = forks.get(*right).unwrap().clone();
        let p = Philosopher {
            name: *name,
            forks: (left, right),
            times_eaten: 0,
            time_thinking: Duration::new(0, 0),
            start_thinking: SystemTime::now(),
            eating_time: Duration::from_millis(10),
        };
        let build = BuildActorOp::new(None, p, Philosopher::handle);
        block_on(handle.new_actor(build)).unwrap()
    })
    .collect::<Vec<Actor>>();

    philosophers.iter().for_each(|p| {
        let _ = p.send_after(StopSession, Duration::from_secs(5));
    });
}
