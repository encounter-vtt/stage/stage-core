# Licensing 
Stage is freely available for use under any of the following licenses:
- [PolyForm-Noncommercial](https://polyformproject.org/licenses/noncommercial/)
- [PolyForm-Small-Business](https://polyformproject.org/licenses/small-business/)

Licenses are available for purchase, for commercial usage that is not covered by the PolyForm-Small-Business license. [Please contact the developer](dev@khionu.net) for more details.
