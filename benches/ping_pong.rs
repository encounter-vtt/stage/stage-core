use arraytools::ArrayTools;
use criterion::{black_box, criterion_group, criterion_main, Criterion};
use futures::executor::block_on;
use stage::actors::{Actor, ActorError, BuildActorOp, Response};
use stage::sys_msgs::{ActorStart, Request};
use stage::system::{ActorSystemBuilder, ActorSystemConfig};
use stage::{
    actor_msg,
    actors::{ActorCtx, ActorResult, Message},
};
use std::sync::atomic::{AtomicU8, Ordering};
use std::sync::Arc;
use std::time::{Duration, Instant};
use tokio::runtime;
use tokio::runtime::Handle;
use tokio::sync::{Barrier, Mutex, Notify, Semaphore};
use tracing::{debug, error, info, trace};

struct PingPong {
    id: u16,
    sum: u16,
    inc: u8,
    targets: Option<[Actor; 3]>,
    targets_src: Arc<Mutex<Vec<(u16, Actor)>>>,
    start: Arc<Barrier>,
    end: Arc<Notify>,
    counter: Arc<AtomicU8>,
    finished: bool,
}

impl PingPong {
    fn new(
        id: u16, targets: Arc<Mutex<Vec<(u16, Actor)>>>, start: Arc<Barrier>, end: Arc<Notify>,
        counter: Arc<AtomicU8>,
    ) -> Self {
        Self {
            id,
            sum: id,
            inc: 0,
            targets: None,
            targets_src: targets,
            start,
            end,
            counter,
            finished: false,
        }
    }

    async fn handle(mut self, ctx: ActorCtx, msg: Message) -> ActorResult<Self> {
        if msg.is::<Setup>() {
            debug!("{:02} is setting up", self.id);
            let targets = self.targets_src.lock().await;
            let new = targets.iter().filter_map(
                |(i, a)| {
                    if *i != self.id {
                        Some(a.clone())
                    } else {
                        None
                    }
                },
            );
            self.targets = <[_; 3]>::from_iter(new);
        } else if msg.is::<Start>() {
            self.sum = self.id;
            self.inc = 0;
            self.finished = false;
            self.start.wait().await;
            debug!("{:02} is starting", self.id);
            for a in self.targets.as_ref().unwrap().iter() {
                let this = ctx.this.clone();
                a.ask(Ping, Response::Mail(this)).await.unwrap();
            }
            debug!("{:02} sent Pings", self.id);
        } else if let Some(req) = msg.try_cast::<Request>() {
            if req.msg().is::<Ping>() {
                trace!("{:02} received a Ping", self.id);
                req.respond(Pong(self.id)).await.unwrap();
                trace!("{:02} sent a Pong", self.id);
            } else {
                error!("{:02} received an unexpected Request: {:?}", self.id, req.msg());
            }
        } else if let Some(pong) = msg.try_cast::<Pong>() {
            self.inc += 1;
            if self.sum & pong.0 != 0 {
                error!("{:02} received a duplicate {:16b}", self.id, pong.0);
            } else {
                self.sum = self.sum | pong.0;
            }
            debug!(
                "{:02} is {:02}/04, sum + {:16b} -> {:16b}",
                self.id, self.inc, pong.0, self.sum
            );
        } else if msg.is::<ActorStart>() {
            trace!("{:02} received ActorStart", self.id);
        } else {
            error!("{:02} received unexpected message, {:?}", self.id, msg);
        }

        if self.inc == 15 {
            if self.sum != u16::max_value() {
                let missing = (0u16..4)
                    .into_iter()
                    .filter_map(|i| {
                        let check = 1u16 << i;
                        if self.sum & check == 0 {
                            Some(check)
                        } else {
                            None
                        }
                    })
                    .map(|i| format!("{:02}", i))
                    .collect::<Vec<String>>()
                    .join(",");
                let missing = String::from("[") + missing.as_str() + "]";
                error!(
                    "{:02} should be completed, but is missing the following: {}",
                    self.id, missing
                );
            } else if !self.finished {
                debug!("{:02} has completed", self.id);
                let _ = self.counter.fetch_add(1, Ordering::Acquire);
                self.finished = true;
                self.end.notify();
            }
        }

        Ok(self)
    }
}

struct Ping;
struct Pong(u16);
struct Setup;
struct Start;

actor_msg!(Ping, Pong, Setup, Start);

pub fn ping_pong(c: &mut Criterion) {
    let sub = tracing_subscriber::FmtSubscriber::builder()
        .with_env_filter("ping_pong=trace,stage=error")
        .finish();

    let _ = tracing::subscriber::set_global_default(sub);

    // stage::trace_panic_hook();

    let mut tokio_rt = runtime::Builder::new()
        .threaded_scheduler()
        .enable_all()
        .build()
        .expect("Failed to build tokio runtime");

    tokio_rt.block_on(async_ping_pong(tokio_rt.handle().clone(), c));
}

pub async fn async_ping_pong(tokio_rt: Handle, c: &mut Criterion) {
    let cfg = ActorSystemConfig::default();
    let sys = ActorSystemBuilder::new(cfg, tokio_rt).start().await.unwrap();

    let handle = sys.get_handle();

    let start = Arc::new(Barrier::new(5));
    let end = Arc::new(Notify::new());
    let counter = Arc::new(AtomicU8::new(0));
    let targets: Arc<Mutex<Vec<(u16, Actor)>>> = Default::default();

    for i in 0u16..4 {
        let build = BuildActorOp::new(
            None,
            PingPong::new(i, targets.clone(), start.clone(), end.clone(), counter.clone()),
            PingPong::handle,
        );
        let actor = handle.new_actor(build).await.unwrap();

        targets.lock().await.push((i, actor));
    }

    let targets: Vec<Actor> = (*targets.lock().await).iter().map(|x| x.1.clone()).collect();

    c.bench_function("ping-pong", |b| {
        block_on(async {
            for a in targets.iter() {
                a.send(Setup).await.unwrap()
            }
        });

        b.iter(|| {
            block_on(async {
                for a in targets.iter() {
                    a.send(Start).await.unwrap();
                }
                start.wait().await;
                loop {
                    end.notified().await;

                    if counter.load(Ordering::Relaxed) >= 4 {
                        let _ = counter.swap(0, Ordering::Acquire);
                        break;
                    }
                }
            })
        });
    });
}

criterion_group!(benches, ping_pong);
criterion_main!(benches);
